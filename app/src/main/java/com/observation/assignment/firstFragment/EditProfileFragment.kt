package com.observation.assignment.firstFragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.observation.assignment.Age
import com.observation.assignment.AgeCalculator.calculateAge
import com.observation.assignment.R
import com.observation.assignment.databinding.ActivityEditProfileBinding
import com.observation.assignment.firstFragment.Validator.isValidDate
import com.observation.assignment.firstFragment.Validator.isValidMonth
import com.observation.assignment.firstFragment.Validator.isValidUserName
import com.observation.assignment.firstFragment.Validator.isValidYear
import com.observation.assignment.secondFragment.ProfileFragment
import com.observation.assignment.secondFragment.ProfileModel
import com.observation.assignment.secondFragment.UserDetails
import com.observation.assignment.splash.viewModel.ProfileViewModel
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class EditProfileFragment : Fragment() {

    private lateinit var dataBinding: ActivityEditProfileBinding
    private var viewModel: ProfileViewModel? = null
    private var model: ProfileModel? = null
    var year: ArrayList<String> = ArrayList()
    var dateBasedOnMonth: ArrayList<String> = ArrayList()
    var monthList: ArrayList<String> = ArrayList()


    companion object {
        fun newInstance(): Fragment {
            return EditProfileFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = ActivityEditProfileBinding.inflate(inflater)
        dataBinding.lifecycleOwner = this

        setViewModel()
        dataBinding.viewModel = viewModel


        return dataBinding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI()

        dataBinding?.viewModel = viewModel
        dataBinding?.lifecycleOwner = this

        model = activity?.run {
            ViewModelProvider(this).get(ProfileModel::class.java)
        } ?: throw Exception("Invalid Activity")

        et_first_name.onChange {
            val isValidName = isValidUserName(it.trim())
            showError(et_first_name, first_name_invalid, !isValidName)
        }

        et_last_name.onChange {
            val isValidName = isValidUserName(it.trim())
            showError(et_last_name, last_name_invalid, !isValidName)
        }
        setYearList()
        updateMonthAdapter()
        updateDateAdapter()

        home_year_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {

                    val isValidYear = isValidYear(
                        home_year_spinner.selectedItem.toString(),
                        resources.getString(R.string.select_year)
                    )
                    showSpinnerError(home_year_spinner, year_invalid, !isValidYear)
                }
            }

        }
        home_month_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    val isValidMonth =
                        isValidMonth(
                            home_month_spinner.selectedItem.toString(),
                            resources.getString(R.string.select_month)
                        )
                    showSpinnerError(home_month_spinner, month_invalid, !isValidMonth)
                }

                if (position === 1 || position === 3 || position === 5 || position === 9 || position === 10 || position === 12) {
                    var datePerParticularMonth = ArrayList(dateBasedOnMonth)
                    val dayAdapter = ArrayAdapter(
                        activity?.applicationContext!!,
                        android.R.layout.simple_spinner_item,
                        datePerParticularMonth
                    )
                    home_date_spinner.adapter = dayAdapter
                } else if (position === 2) {
                    var datePerParticularMonth = ArrayList(dateBasedOnMonth!!.subList(0, 29))
                    val dayAdapter = ArrayAdapter(
                        activity?.applicationContext!!,
                        android.R.layout.simple_spinner_item,
                        datePerParticularMonth
                    )
                    home_date_spinner.adapter = dayAdapter
                } else {
                    var datePerParticularMonth = ArrayList(dateBasedOnMonth!!.subList(0, 31))
                    val dayAdapter = ArrayAdapter(
                        activity?.applicationContext!!,
                        android.R.layout.simple_spinner_item,
                        datePerParticularMonth
                    )
                    home_date_spinner.adapter = dayAdapter
                }
            }

        }
        home_date_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    val isValidDate =
                        isValidDate(
                            home_date_spinner.selectedItem.toString(),
                            resources.getString(R.string.select_date)
                        )
                    showSpinnerError(home_date_spinner, date_invalid, !isValidDate)
                }

            }

        }
        btn_save_profile.setOnClickListener {
            if (isValidData()) {
                val selectedYear = Integer.parseInt(year[home_year_spinner.selectedItemPosition])
                val selectedMonth = home_month_spinner.selectedItemPosition
                val selectedDate =
                    Integer.parseInt(dateBasedOnMonth[home_date_spinner.selectedItemPosition])
                val sdf = SimpleDateFormat("dd/MM/yyyy")
                val birthDate: Date = sdf.parse("$selectedDate/$selectedMonth/$selectedYear")
                val age: Age = calculateAge(birthDate)
                model?.setUserDetails(
                    UserDetails(
                        et_first_name.text.toString(),
                        et_last_name.text.toString(), age.toString()
                    )
                )
                openFragment(ProfileFragment.newInstance())

            }
        }

    }

    private fun setYearList() {
        val thisYear: Int = Calendar.getInstance().get(Calendar.YEAR)
        year.add(resources.getString(R.string.select_year))

        for (i in 1960..thisYear) {
            year.add(i.toString())
        }
        updateYearAdapter(year)

    }

    private fun setViewModel() {
        viewModel = ProfileViewModel()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun isValidData(): Boolean {
        val isValidName = isValidUserName(et_first_name.text.trim().toString())
        val isValidLastName = isValidUserName(et_last_name.text.toString())
        val isValidYear = isValidYear(
            home_year_spinner.selectedItem.toString(),
            resources.getString(R.string.select_year)
        )
        val isValidMonth =
            isValidMonth(
                home_month_spinner.selectedItem.toString(),
                resources.getString(R.string.select_month)
            )
        val isValidDate =
            isValidDate(
                home_date_spinner.selectedItem.toString(),
                resources.getString(R.string.select_date)
            )

        showError(et_first_name, first_name_invalid, !isValidName)
        showError(et_last_name, last_name_invalid, !isValidLastName)
        showSpinnerError(home_year_spinner, year_invalid, !isValidYear)
        showSpinnerError(home_month_spinner, month_invalid, !isValidMonth)
        showSpinnerError(home_date_spinner, date_invalid, !isValidDate)

        return isValidName && isValidLastName && isValidYear && isValidMonth && isValidDate
    }

    private fun updateUI() {

    }

    private fun updateYearAdapter(cities: ArrayList<String>) {

        val yearAdapter: ArrayAdapter<String> =
            ArrayAdapter(
                activity?.applicationContext!!,
                android.R.layout.simple_spinner_item,
                cities
            )
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        home_year_spinner.adapter = yearAdapter

    }

    private fun updateMonthAdapter() {
        val monthStringArray =
            resources.getStringArray(R.array.item_month)
        monthList.add(resources.getString(R.string.select_month))
        for (i in monthStringArray.indices) {
            monthList!!.add(monthStringArray[i].toString())
        }
        val monthAdapter: ArrayAdapter<String> =
            ArrayAdapter(
                activity?.applicationContext!!,
                android.R.layout.simple_spinner_item,
                monthList
            )
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        home_month_spinner.adapter = monthAdapter

    }

    private fun updateDateAdapter() {
        dateBasedOnMonth.add(resources.getString(R.string.select_date))
        val dateStringArray =
            resources.getStringArray(R.array.item_day)
        for (i in dateStringArray.indices) {
            dateBasedOnMonth!!.add(dateStringArray[i].toString())
        }
        val dateAdapter: ArrayAdapter<String> =
            ArrayAdapter(
                activity?.applicationContext!!,
                android.R.layout.simple_spinner_item,
                dateBasedOnMonth
            )
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        home_date_spinner.adapter = dateAdapter

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showError(
        editText: EditText,
        textView: TextView,
        show: Boolean
    ) {
        if (show) {
            textView.visibility = View.VISIBLE
            editText.background =
                this.resources.getDrawable(R.drawable.error_edit_bg, activity?.theme)
        } else {
            textView.visibility = View.GONE
            editText.background =
                this.resources.getDrawable(R.drawable.edit_text_bg, activity?.theme)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showSpinnerError(
        spinner: Spinner,
        textView: TextView,
        show: Boolean
    ) {
        if (show) {
            textView.visibility = View.VISIBLE
            spinner.background =
                this.resources.getDrawable(R.drawable.error_edit_bg, activity?.theme)
        } else {
            textView.visibility = View.GONE
            spinner.background =
                this.resources.getDrawable(R.drawable.edit_text_bg, activity?.theme)
        }
    }

    private fun openFragment(newInstance: Fragment) {

        //uncomment pass data directly from one fragment to another
        /*val ageInt = calculateAge()
        val args = Bundle()
        args.putString("age", ageInt.toString())
        args.putString("FirstName", et_first_name.text.toString())
        args.putString("LastName", et_last_name.text.toString())
        newInstance.arguments = args
         */
        val transaction: FragmentTransaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.main_container, newInstance)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}