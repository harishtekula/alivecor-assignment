//package com.observation.assignment.firstFragment
//
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.content.Intent
//import android.content.IntentFilter
//import android.os.Bundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import android.widget.Toast
//import androidx.fragment.app.Fragment
//import androidx.lifecycle.Observer
//import androidx.localbroadcastmanager.content.LocalBroadcastManager
//import com.garage.mechanic.const.Const
//import com.garage.mechanic.databinding.HomeBinding
//import com.garage.mechanic.model.Content
//import com.garage.mechanic.order.OrderActivity
//import com.garage.mechanic.order.OrderAdapter
//import com.garage.mechanic.order.viewmodel.OrderViewModel
//import com.garage.mechanic.prefs.PreferenceManager
//import com.garage.mechanic.repo.UserRepositoryImpl
//
//class HomeFragment : Fragment(), OrderAdapter.OrderClickListener {
//
//    private var adapter: OrderAdapter? = null
//    private var viewModel: OrderViewModel? = null
//    private lateinit var dataBinding: HomeBinding
//
//    companion object {
//        fun newInstance(): Fragment {
//            return HomeFragment()
//        }
//    }
//    private val receiver = object : BroadcastReceiver() {
//        override fun onReceive(context: Context?, intent: Intent?) {
//            if (intent != null) {
//            }
//        }
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        dataBinding = HomeBinding.inflate(inflater)
//        dataBinding.lifecycleOwner = this
//        setViewModel()
//        dataBinding.viewModel = viewModel
//        prefs = PreferenceManager(requireActivity())
//        viewModel?.callOrders(prefs.getAccessToken()!!, prefs.getPhoneNumber()!!, Const.PENDING)
//
//        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
//            receiver,
//            IntentFilter("order_refresh")
//        )
//
//        setUpObserver()
//        return dataBinding.root
//    }
//
//    private fun setUpObserver() {
//        viewModel?.getOrder()?.observe(viewLifecycleOwner, Observer {
//            Toast.makeText(context, "Booking has been Rejected", Toast.LENGTH_SHORT).show()
//            viewModel?.callOrders(prefs.getAccessToken()!!, prefs.getPhoneNumber()!!, Const.PENDING)
//        })
//    }
//
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        adapter = OrderAdapter(requireActivity(), this)
//        dataBinding.rvOrders.adapter = adapter
//    }
//
//
//    private fun setViewModel() {
//        viewModel = OrderViewModel(UserRepositoryImpl())
//    }
//
//    override fun onOrderClick(order: Content) {
//        val intent = Intent(requireContext(), OrderActivity::class.java)
//        intent.putExtra("orderId", order.id)
//        startActivity(intent)
//    }
//
//    override fun onOrderCancelled(order: Content) {
//        viewModel?.rejectOrder(
//            prefs.getAccessToken()!!,
//            order.id,
//            prefs.getPhoneNumber()!!
//        )
//    }
//
//    override fun onDestroyView() {
//        super.onDestroyView()
//        context?.let { LocalBroadcastManager.getInstance(it).unregisterReceiver(receiver) }
//    }
//}
