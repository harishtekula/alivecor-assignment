package com.observation.assignment.splash.viewModel

import androidx.lifecycle.*

class ProfileViewModel() : ViewModel() {
    private val isSuccess: MutableLiveData<Boolean> = MutableLiveData()


    private fun setSuccess() {
        isSuccess.value = true
    }

    private fun setError() {
        isSuccess.value = false
    }

    private fun setLoading() {
        isSuccess.value = false
    }

    public override fun onCleared() {
        super.onCleared()
    }

}