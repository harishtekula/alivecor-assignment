package com.observation.assignment.firstFragment

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog

import com.google.android.material.snackbar.Snackbar
import com.observation.assignment.R

fun EditText.onChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s!!.isNotEmpty()) {
                cb(s.toString())
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

fun Activity.hideKeyboard() {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.currentFocus;
    if (view == null) {
        view = View(this);
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0);
}

fun EditText.onlyUppercase() {
    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
    filters = arrayOf(InputFilter.AllCaps())
}

fun View.snackbar(message: String?) {
    Snackbar.make(this, message!!, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
    }.show()
}

fun EditText.onSearchChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            cb(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // this is not need because we need to validate once entering text is done
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            // this is not need because we need to validate once entering text is done
        }
    })
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
private fun Context.showError(layout: EditText, textView: TextView, show: Boolean) {
    if (show) {
        textView.visibility = View.VISIBLE
        layout.background = this.resources.getDrawable(R.drawable.error_edit_bg, this.theme)
    } else {
        textView.visibility = View.GONE
        layout.background = this.resources.getDrawable(R.drawable.edit_text_bg, this.theme)
    }
}

fun EditText.onTextChange(cb: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            cb(s.toString())
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.showError(
    editText: EditText,
    textView: TextView,
    show: Boolean
) {
    if (show) {
        textView.visibility = View.VISIBLE
        editText.background = this.resources.getDrawable(R.drawable.error_edit_bg, theme)
    } else {
        textView.visibility = View.GONE
        editText.background = this.resources.getDrawable(R.drawable.edit_text_bg, theme)
    }
}



fun Context.showInfoAlert(message: String) {
    AlertDialog.Builder(this)
        .setMessage(message)
        .setNegativeButton(
            "Ok"
        ) { dialog, _ -> dialog.dismiss() }.show()
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun isOnline(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    if (connectivityManager != null) {
        val capabilities =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            } else {
                TODO("VERSION.SDK_INT < M")
            }
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
    }
    return false
}

fun showKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun TextView.createLink(
    completeString: String,
    partToClick: String, clickableAction: ClickableSpan?
) {
    val spannableString = SpannableString(completeString)
    val startPosition = completeString.indexOf(partToClick)
    val endPosition = completeString.lastIndexOf(partToClick) + partToClick.length
    spannableString.setSpan(
        clickableAction, startPosition, endPosition,
        Spanned.SPAN_INCLUSIVE_EXCLUSIVE
    )
    this.text = spannableString
    this.movementMethod = LinkMovementMethod.getInstance()
}



