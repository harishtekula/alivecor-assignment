package com.observation.assignment.secondFragment


data class UserDetails(
    var firstName: String,
    var lastName: String,
    var age: String
)