package com.observation.assignment.secondFragment

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.observation.assignment.R
import kotlinx.android.synthetic.main.activity_profile.*


// Fragment to obtain values from fragment 1
class ProfileFragment : Fragment() {
    private var model: ProfileModel? = null

    companion object {
        fun newInstance(): Fragment {
            return ProfileFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProvider(this).get(ProfileModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_profile, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //receive values passed as bundle form Fragment1
        /* val firstName = requireArguments().getString("FirstName")
         val lastName = requireArguments().getString("LastName")
         val age = requireArguments().getString("age")
      */

        model?.getUserDetails()?.observe(viewLifecycleOwner, Observer {

            if (it.firstName.isNotEmpty()) {
                Log.v("ProfileModel  age%S", "" + it.age)
                tv_profile_details.text = resources.getString(
                    R.string.user_profile_details,
                    it.firstName,
                    it.lastName,
                    it.age.toString()
                )
            }
        })

    }


}