package com.observation.assignment.splash.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.viewModelScope
import com.observation.assignment.MainActivity
import com.observation.assignment.R
import com.observation.assignment.splash.viewModel.SplashViewModel
import kotlinx.coroutines.launch

class SplashActivity : AppCompatActivity(), SplashViewModel.SplashListener {

    private val model: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        model.setListener(this)
        model.viewModelScope.launch { model.startNextActivity() }
    }

    override fun openLoginActivity() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()

    }

    private fun openFragment(newInstance: Fragment) {
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, newInstance)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}