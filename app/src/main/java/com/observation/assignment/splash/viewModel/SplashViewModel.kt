package com.observation.assignment.splash.viewModel

import android.os.Handler
import androidx.lifecycle.ViewModel

class SplashViewModel : ViewModel() {

    fun startNextActivity() {
        Handler().postDelayed({
            splashListener.openLoginActivity()
        }, 2000)
    }

    private lateinit var splashListener: SplashListener

    fun setListener(splashListener: SplashListener) {
        this.splashListener = splashListener
    }

    interface SplashListener {
        fun openLoginActivity()
    }
}