package com.observation.assignment.firstFragment

object Validator {

    fun isValidYear(username: String, validator: String): Boolean {
        return username.isNotEmpty() && username != validator
    }

    fun isValidMonth(username: String, validator: String): Boolean {
        return username.isNotEmpty() && username != validator
    }

    fun isValidDate(username: String, validator: String): Boolean {
        return username.isNotEmpty() && username != validator
    }

    fun isValidUserName(username: String): Boolean {
        return username.isNotEmpty() && username.length >= 1
    }



}