package com.observation.assignment.secondFragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel



// Setting Profile Model to update the fragment 2 with values from fragment 1
class ProfileModel : ViewModel() {
    private val firstName = MutableLiveData<String>()
    private val lastName = MutableLiveData<String>()
    private val age = MutableLiveData<String>()
    private val userDetails = MutableLiveData<UserDetails>()


    fun setAge(number: String) {
        age.value = number
    }

    fun getAge(): MutableLiveData<String> {
        return age;
    }

    fun setUserDetails(userDetail: UserDetails) {
        userDetails.value = userDetail
    }

    fun getUserDetails(): MutableLiveData<UserDetails> {
        return userDetails;
    }

    fun getFirstName(): MutableLiveData<String> {
        return firstName
    }

    fun getLastName(): MutableLiveData<String> {
        return lastName
    }

    fun setFirstName(firstNameData: String) {
        firstName.value = firstNameData
    }

    fun setLastName(lastNameData: String) {
        lastName.value = lastNameData
    }
}